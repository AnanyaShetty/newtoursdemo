package com.newtours.testcases;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.openqa.selenium.WebDriver;
//import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.newtours.base.BaseSetUp;
import com.newtours.dataprovider.BookAFlightPageData;
import com.newtours.dataprovider.FlightFinderPageData;
import com.newtours.dataprovider.InputDataFromExcel;
import com.newtours.dataprovider.RegisterPageData;
import com.newtours.dataprovider.SelectFlightPageData;
import com.newtours.pageobjects.BookAFlightPage;
import com.newtours.pageobjects.FlightFinderPage;
import com.newtours.pageobjects.RegisterPage;
import com.newtours.pageobjects.SelectFlightPage;
import com.newtours.pageobjects.SignInPage;

public class NewToursTest extends BaseSetUp {
	
	WebDriver driver;

	@BeforeMethod
	public void setUp() {
		System.out.println("Before test");
		driver =getDriver();
		System.out.println("get driver before test"+driver);
	}
	
	//@Test
	//public void tests(){
	//	suite.testBookFlightSuite1();
	//}
	
	@Test
	public void testBookFlightSuite1() throws Exception{
		try{
		InputDataFromExcel id=new InputDataFromExcel();
		XSSFRow row = id.inputData();
		
		RegisterPageData register= new RegisterPageData();
		register.setFirstName(row.getCell(0).getStringCellValue());
		register.setLastName(row.getCell(1).getStringCellValue());
		register.setPhone(String.valueOf(row.getCell(2).getNumericCellValue()));
		register.setEmail(row.getCell(3).getStringCellValue());
		register.setAddress1(row.getCell(4).getStringCellValue());
		register.setAddress2(row.getCell(5).getStringCellValue());
		register.setCity(row.getCell(6).getStringCellValue());
		register.setState(row.getCell(7).getStringCellValue());
		register.setPostalCode(String.valueOf(row.getCell(8).getNumericCellValue()));
		register.setCountry(row.getCell(9).getStringCellValue());
		register.setUserName(row.getCell(10).getStringCellValue());
		register.setPassword(row.getCell(11).getStringCellValue());
		register.setConfirmPassword(row.getCell(12).getStringCellValue());
		
		RegisterPage regpage=new RegisterPage(register);
		regpage.runActions();
		
		SignInPage signin=new SignInPage();
		signin.runActions();
		
		FlightFinderPageData flightfind = new FlightFinderPageData();
		flightfind.setType("One Way");
		flightfind.setPassenger("2");
		flightfind.setDepartingFrom("New York");
		flightfind.setDate("30");
		flightfind.setMonth("September");
		flightfind.setArrivingIn("Portland");
		flightfind.setReturnDate("10");
		flightfind.setReturnMonth("October");
		flightfind.setServiceClass("Business Class");
		flightfind.setAirline("Blue Skies Airlines");
		
		FlightFinderPage searchflights = new FlightFinderPage(flightfind);
		searchflights.runActions();
		
		SelectFlightPageData flightdata = new SelectFlightPageData();
		flightdata.setDepart("Blue Skies Airlines 361");
		flightdata.setReturnFlight("Unified Airlines 633");
		
		SelectFlightPage selectflight = new SelectFlightPage(flightdata);
		selectflight.runActions();
		
		BookAFlightPageData bookdata = new BookAFlightPageData();
		bookdata.setFirstName("test1");
		bookdata.setLastName("test1");
		bookdata.setMeal("Hindu");
		bookdata.setCardType("Visa");
		bookdata.setCardNumber("4200000000");
		bookdata.setCardExpMonth("05");
		bookdata.setCardExpYear("2010");
		bookdata.setCardFname("test1");
		bookdata.setCardMname("test");
		bookdata.setCardLname("test1");
		
		BookAFlightPage bookflight= new BookAFlightPage(bookdata);
		bookflight.runActions();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		driver.close();
		
		}
}
