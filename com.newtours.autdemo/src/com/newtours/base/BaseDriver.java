package com.newtours.base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public abstract class BaseDriver {

	WebDriver driver = null;
	
	public BaseDriver(){
		driver = BaseSetUp.getDriver();
		PageFactory.initElements(driver, this);
	}
	
	protected abstract void runActions();
	
}
