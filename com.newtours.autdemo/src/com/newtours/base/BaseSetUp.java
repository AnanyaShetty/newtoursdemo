package com.newtours.base;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

public class BaseSetUp {
	
	private static WebDriver driver;
	
	public static WebDriver getDriver(){
		return driver;
	}
	
	@Parameters({"browserType"}) // this is passed in TestNG xml
	@BeforeClass
	public static WebDriver openBrowser(String browserType){
		setDriver(browserType);
		System.out.println(driver);
		return driver;
	}
	
	public static WebDriver setDriver(String browserType){
		switch(browserType){
		case "chrome":
			 initializeChromeBrowser();
			 System.out.println("Launching Chrome browser");
			 break;
		case "firefox":
			 initializeFirefoxBrowser();
			 System.out.println("Launching Firefox browser");
			 break;
		case "default":
			System.out.println("Please enter valid browser name either 'chrome' or 'firefox'");
		}
		return driver;
	}

	public static WebDriver initializeChromeBrowser(){
		String pwd =System.getProperty("user.dir");
		System.setProperty("webdriver.chrome.driver", pwd+"//Softwares//chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://newtours.demoaut.com");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		return driver;
	}
	
	public static WebDriver initializeFirefoxBrowser(){
		String pwd =System.getProperty("user.dir");
		System.setProperty("webdriver.firefox.driver", pwd+"//Softwares//geckodriver.exe");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("http://newtours.demoaut.com");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		return driver;
	}
	
	
}
