package com.newtours.pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.newtours.base.BaseDriver;

public class SignInPage extends BaseDriver {

	@FindBy (linkText="Flights")
	WebElement flights;
	
	public void ClickFlights(){
		flights.click();
	}
	
	@Override
	public void runActions(){
		ClickFlights();
	}
	
}
