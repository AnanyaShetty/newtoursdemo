package com.newtours.pageobjects;


//import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
//import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.newtours.base.BaseDriver;
//import com.newtours.base.BaseDriver;
//import com.newtours.base.BaseSetUp;
import com.newtours.dataprovider.RegisterPageData;

public class RegisterPage extends BaseDriver {

	
	//Create WebElement locators
	/*
	public RegisterPage(WebDriver driver){
		System.out.println("before get driver");
		//driver = BaseSetUp.getDriver();
		System.out.println("after get driver");
		PageFactory.initElements(driver, this);
	}*/
	
	@FindBy (linkText="REGISTER")
	WebElement register;

	@FindBy (name="firstName")
	WebElement firstname;
	
	@FindBy (name="lastName")
	WebElement lastname;
	
	@FindBy (name="phone")
	WebElement phonenumber;
	
	@FindBy (name="userName")
	WebElement username;
	
	@FindBy (name="address1")
	WebElement addressline1;
	
	@FindBy (name="address2")
	WebElement addressline2;
	
	@FindBy (name="city")
	WebElement cityname;
	
	@FindBy (name="state")
	WebElement statename;
	
	@FindBy (name="postalCode")
	WebElement postalcode;
	
	@FindBy (name="country")
	WebElement countryname;
	
	@FindBy (name="email")
	WebElement emailid;
	
	@FindBy (name="password")
	WebElement enterpassword;
	
	@FindBy (name="confirmPassword")
	WebElement confirmpassword;
	
	@FindBy (name="register")
	WebElement submit;
	
	/*private By firstName = By.cssSelector("#firstName");
	private By lastName = By.cssSelector("#lastName");
	private By phone = By.cssSelector("#phone");
	private By email=By.cssSelector("#userName");
	private By address1=By.cssSelector("#address1");
	private By address2=By.cssSelector("#address2");
	private By city=By.cssSelector("#city");
	private By state = By.cssSelector("#state");
	private By country= By.cssSelector("#country");
	
	private By userName=By.cssSelector("#email");
	private By password=By.cssSelector("#password");
	private By confirmPassword=By.cssSelector("confirmPassword"); */
	
	private RegisterPageData registerdata; // initialize register data
	
	//fetch values, assign value to register data
	public RegisterPage(RegisterPageData registerdata){
		this.registerdata=registerdata;
	}
	
	// Methods for every user actions

	public void clickregister(){
		System.out.println("Inside click register");
		register.click();
		System.out.println("clicked");
	}
	
	public void setFirstName(String firstName){
	 firstname.sendKeys(firstName);
	}
	
	public void setLastName(String lastName){
		lastname.sendKeys(lastName);
	}
	
	public void setPhone(String phone){
		phonenumber.sendKeys(phone);
	}

	public void setEmail(String email){
		username.sendKeys(email);
	}
	
	public void setAddress1(String address1){
		addressline1.sendKeys(address1);
	}
	
	public void setAddress2(String address2){
		addressline2.sendKeys(address2);
	}
	
	public void setCity(String city){
		cityname.sendKeys(city);
	}
	
	public void setState(String state){
		statename.sendKeys(state);
	}
	
	public void setPostalCode(String postalCode){
		postalcode.sendKeys(postalCode);
	}
	
	public void setCountry(String country){
		System.out.println("select country");
		Select c=new Select(countryname);
		//c.selectByValue(country);
		c.selectByVisibleText(country);
		System.out.println("value"+country);
	}
	
	public void setUserName(String userName){
		emailid.sendKeys(userName);
	}	
	public void setPassword(String password){
		enterpassword.sendKeys(password);
	}	
	public void setConfirmPassword(String confirmPassword){
		confirmpassword.sendKeys(confirmPassword);
	}
	
	public void submit(){
		submit.click();
	}

	@Override
	public void runActions(){
		System.out.println("Before click register");
		clickregister();
		System.out.println("After click reg");
		setFirstName(this.registerdata.getFirstName());
		setLastName(this.registerdata.getLastName());
		setPhone(this.registerdata.getPhone());
		setEmail(this.registerdata.getEmail());
		setAddress1(this.registerdata.getAddress1());
		setAddress2(this.registerdata.getAddress2());
		setCity(this.registerdata.getCity());
		setState(this.registerdata.getState());
		setPostalCode(this.registerdata.getPostalCode());
		setCountry(this.registerdata.getCountry());
		setUserName(this.registerdata.getUserName());
		setPassword(this.registerdata.getPassword());
		setConfirmPassword(this.registerdata.getConfirmPassword());
		submit();
	}
	
}



