package com.newtours.pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import com.newtours.base.BaseDriver;
import com.newtours.dataprovider.FlightFinderPageData;


public class FlightFinderPage extends BaseDriver {
	
	@FindBy (name="tripType")
	List<WebElement> tripType;
	
	@FindBy (name="passCount")
	WebElement passengers;
	
	@FindBy (name="fromPort")
	WebElement departingFrom;
	
	@FindBy (name="fromMonth")
	WebElement month;
	
	@FindBy (name="fromDay")
	WebElement date;
	
	@FindBy (name="toPort")
	WebElement arrivingIn;
	
	@FindBy (name = "toMonth")
	WebElement returnMonth;
	
	@FindBy (name = "toDay")
	WebElement returnDate;
	
	@FindBy (name = "servClass")
	List<WebElement> serviceClass;
	
	@FindBy (name = "airline")
	WebElement selectairline;
	
	@FindBy (name = "findFlights")
	WebElement searchflights;
	
	
	private FlightFinderPageData flightdata;
	public FlightFinderPage(FlightFinderPageData flightdata){
		this.flightdata=flightdata;
	}

		
	public void setType(String trip){
		
		if(trip.equalsIgnoreCase("Round Trip")){
			tripType.get(0).click();
		}else{
			tripType.get(1).click();
		}
		
	}
	
	public void setPassenger(String passenger){
		Select pass = new Select(passengers);
		pass.selectByVisibleText(passenger);
	}
	
	public void setDepartingFrom(String departfrom){
		Select depart = new Select(departingFrom);
		depart.selectByVisibleText(departfrom);
	}
	
	public void setDate(String fromdate){
		Select fromday = new Select(date);
		fromday.selectByVisibleText(fromdate);
	}
	
	public void setMonth(String frommonth){
		System.out.println("in month");
		Select frommon = new Select(month);
		frommon.selectByVisibleText(frommonth);
	}
	
	public void setArrivingIn(String arrive){
		System.out.println("in arrive");
		Select arriving = new Select(arrivingIn);
		arriving.selectByVisibleText(arrive);
		System.out.println("select arrive"+arrive);
	}

	public void setReturnDate(String returndate){
		Select todate = new Select(returnDate);
		todate.selectByVisibleText(returndate);
	}
	
	public void setReturnMonth(String returnmonth){
		Select tomonth = new Select(returnMonth);
		tomonth.selectByVisibleText(returnmonth);
	}
	
	public void setServiceClass(String servClass){
		
		if(servClass.equalsIgnoreCase("Economy class")){
			serviceClass.get(0).click();
		}else if(servClass.equalsIgnoreCase("Business Class")){
			serviceClass.get(1).click();
		}else
			serviceClass.get(2).click();
	}
	
	public void setAirline(String airline){
		Select airLine = new Select(selectairline);
		airLine.selectByVisibleText(airline);
	}
	

	public void Continue(){
		searchflights.click();
	}
	
	@Override
	public void runActions(){
		setType(this.flightdata.getType());
		setPassenger(this.flightdata.getPassenger());
		setDepartingFrom(this.flightdata.getDepartingFrom());
		setDate(this.flightdata.getDate());
		setMonth(this.flightdata.getMonth());
		setArrivingIn(this.flightdata.getArrivingIn());
		setReturnDate(this.flightdata.getReturnDate());
		setReturnMonth(this.flightdata.getReturnMonth());
		setServiceClass(this.flightdata.getServiceClass());
		setAirline(this.flightdata.getAirline());
		Continue();		
	}
	
}
