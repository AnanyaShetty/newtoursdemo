package com.newtours.pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.newtours.base.BaseDriver;
import com.newtours.dataprovider.SelectFlightPageData;

public class SelectFlightPage extends BaseDriver {
	
	@FindBy (name = "outFlight")
	List<WebElement> outFlight;
	
	@FindBy (name = "inFlight")
	List<WebElement> inFlight;

	@FindBy (name= "reserveFlights")
	WebElement reserve;
	
	public void Depart(String departure){
         
		if (departure.equalsIgnoreCase("Blue Skies Airlines 360")){
			outFlight.get(0).click();
		}else if (departure.equalsIgnoreCase("Blue Skies Airlines 361")){
			outFlight.get(1).click();
	    }else if (departure.equalsIgnoreCase("Pangaea Airlines 362")){
		   outFlight.get(2).click();
	    }else 
	    	outFlight.get(3).click();
	}
	
	public void Return(String returnFlight){
        
		if (returnFlight.equalsIgnoreCase("Blue Skies Airlines 630")){
			inFlight.get(0).click();
		}else if (returnFlight.equalsIgnoreCase("Blue Skies Airlines 631")){
			inFlight.get(1).click();
	    }else if (returnFlight.equalsIgnoreCase("Pangaea Airlines 632")){
		   inFlight.get(2).click();
	    }else 
	    	inFlight.get(3).click();
	}
	
	public void ReserveFlight(){
		System.out.println("check flight");
		reserve.click();
	}
	
	private SelectFlightPageData selectflight;
	public SelectFlightPage(SelectFlightPageData selectflight){
		this.selectflight=selectflight;
	}
	
	@Override
	public void runActions(){
		Depart(this.selectflight.getDepart());
		Return(this.selectflight.getReturnFlight());
		ReserveFlight();
	}	
}
