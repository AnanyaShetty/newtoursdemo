package com.newtours.pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import com.newtours.base.BaseDriver;
import com.newtours.dataprovider.BookAFlightPageData;

public class BookAFlightPage extends BaseDriver {

	@FindBy (name="passFirst0")
	WebElement passfname;
	
	@FindBy (name="passLast0")
	WebElement passlname;
	
	@FindBy (name="pass.0.meal")
	WebElement passmeal;
	
	@FindBy (name="creditCard")
	WebElement cardtype;
	
	@FindBy (name="creditnumber")
	WebElement cardnumber;
	
	@FindBy (name="cc_exp_dt_mn")
	WebElement cardexpmonth;
	
	@FindBy (name="cc_exp_dt_yr")
	WebElement cardexpyear;
	
	@FindBy (name="cc_frst_name")
	WebElement ccfname;
	
	@FindBy (name="cc_mid_name")
	WebElement ccmname;
	
	@FindBy (name="cc_last_name")
	WebElement cclname;
	
	@FindBy (name= "buyFlights")
	WebElement purchase;
	
	private BookAFlightPageData flightpage;
	public BookAFlightPage(BookAFlightPageData flightpage){
		this.flightpage=flightpage;
	}

	public void setFirstName(String firstname){
		passfname.sendKeys(firstname);
	}
	
	public void setLastName(String lastname){
		passlname.sendKeys(lastname);
	}
	public void setMeal(String meal){
		Select m=new Select(passmeal);
		m.selectByVisibleText(meal);
	}
	
	public void setCardType(String type){
		Select t=new Select(cardtype);
		t.selectByVisibleText(type);
	}
	
	public void setCardNumber(String cardno){
		cardnumber.sendKeys(cardno);
	}
	
	public void setCardExpMonth(String cardexpmon){
		Select expmon=new Select(cardexpmonth);
		expmon.selectByVisibleText(cardexpmon);
	}
	
	public void setCardExpYear(String cardexpyr){
		Select expyear=new Select(cardexpyear);
		expyear.selectByVisibleText(cardexpyr);
	}
	
	public void setCardFname(String cardfname){
		ccfname.sendKeys(cardfname);
	}
	public void setCardMname(String cardmname){
		ccmname.sendKeys(cardmname);
	}
	
	public void setCardLname(String cardlname){
		cclname.sendKeys(cardlname);
	}

	public void clickPurchase(){
		purchase.click();
	}

	@Override
	public void runActions(){
		setFirstName(this.flightpage.getFirstName());
		setLastName(this.flightpage.getLastName());
		setMeal(this.flightpage.getMeal());
		setCardType(this.flightpage.getCardType());
		setCardNumber(this.flightpage.getCardNumber());
		setCardExpMonth(this.flightpage.getCardExpMonth());
		setCardExpYear(this.flightpage.getCardExpYear());
		setCardFname(this.flightpage.getCardFname());
		setCardMname(this.flightpage.getCardMname());
		setCardLname(this.flightpage.getCardLname());
		clickPurchase();
	}
	
}