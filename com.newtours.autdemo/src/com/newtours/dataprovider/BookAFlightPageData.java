package com.newtours.dataprovider;

public class BookAFlightPageData {
	
	private String firstname;
	private String lastname;
	private String meal;
	private String type;
	private String cardno;
	private String cardexpmonth;
	private String cardexpyear;
	private String cardfname;
	private String cardmname;
	private String cardlname;
	
	
	public void setFirstName(String firstname){
		this.firstname=firstname;
	}
	
	public void setLastName(String lastname){
		this.lastname=lastname;
	}
	
	public void setMeal(String meal){
		this.meal=meal;
	}
	
	public void  setCardType(String type){
		this.type=type;
	}
	
	public void setCardNumber(String cardno){
		this.cardno=cardno;
	}
	
	public void setCardExpMonth(String cardexpmonth){
		this.cardexpmonth=cardexpmonth;
	}
	
	public void setCardExpYear(String cardexpyear){
		this.cardexpyear=cardexpyear;
	}
	
	public void setCardFname(String cardfname){
		this.cardfname=cardfname;
	}
	public void setCardMname(String cardmname){
		this.cardmname=cardmname;
	}
	public void setCardLname(String cardlname){
		this.cardlname=cardlname;
	}
	
	public String getFirstName(){
		return firstname;
	}
	
	public String getLastName(){
		return lastname;
	}
	
	public String getMeal(){
		return meal;
	}
	
	public String getCardType(){
		return type;
	}
	
	public String getCardNumber(){
		return cardno;
	}
	
	public String getCardExpMonth(){
		return cardexpmonth;
	}
	
	public String getCardExpYear(){
		return cardexpyear;
	}
	
	public String getCardFname(){
		return cardfname;
	}
	public String getCardMname(){
		return cardmname;
	}
	public String getCardLname(){
		return cardlname;
	}
	

}
