package com.newtours.dataprovider;

public class RegisterPageData {

	private String firstName;
	private String lastName;
	private String phone;
	private String email;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String postalCode;
	private String country;
	private String userName;
	private String password;
	private String confirmpassword;
	
	// get all values from main test suite
	public String getFirstName(){
		return firstName;
	}
	
	public String getLastName(){
		return lastName;
	}
	
	public String getPhone(){
		return phone;
	}
	
	public String getEmail(){
		return email;
	}
	
	public String getAddress1(){
		return address1;
	}
	
	public String getAddress2(){
		return address2;
	}
	
	public String getCity(){
		return city;
	}
	
	public String getState(){
		return state;
	}
	
	public String getPostalCode(){
		return postalCode;
	}

	public String getCountry(){
		return country;
	}

	public String getUserName(){
		return userName;
	}
	
	public String getPassword(){
		return password;
	}
	
	public String getConfirmPassword(){
		return confirmpassword;
	}
	
	//set the values received
	
	public void setFirstName(String firstName){
		this.firstName= firstName;
	}
	
	public void setLastName(String lastName){
		this.lastName= lastName;
	}
	
	public void setPhone(String phone){
		this.phone=phone;
	}
	
	public void setEmail(String email){
		this.email=email;
	}
	
	public void setAddress1(String address1){
		this.address1= address1;
	}
	
	public void setAddress2(String address2){
		this.address2=address2;
	}
	
	public void setCity(String city){
		this.city=city;
	}
	
	public void setState(String state){
		this.state=state;
	}
	
	public void setPostalCode(String postalCode){
		this.postalCode=postalCode;
	}

	public void setCountry(String country){
		this.country=country;
	}

	public void setUserName(String userName){
		this.userName=userName;
	}
	
	public void setPassword(String password){
		this.password=password;
	}
	
	public void setConfirmPassword(String confirmpassword){
		this.confirmpassword=confirmpassword;
	}
	
}



	
