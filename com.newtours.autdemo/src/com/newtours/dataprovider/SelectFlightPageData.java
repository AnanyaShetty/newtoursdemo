package com.newtours.dataprovider;

public class SelectFlightPageData {

	private String departure;
	private String returnFlight;
		
	public void setDepart(String departure){
		this.departure=departure;
		}
	
	public void setReturnFlight(String returnFlight){
		this.returnFlight=returnFlight;
	}
	
	public String getDepart(){
		return departure;
	}
	
	public String getReturnFlight(){
		return returnFlight;
	}
}

