package com.newtours.dataprovider;

import java.io.File;
import java.io.FileInputStream;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class InputDataFromExcel {

	public XSSFRow inputData() throws Exception {
		File src = new File("C://Users//Win10//workspace//LearnJava//Softwares//newtoursData.xlsx");
		try (FileInputStream fis = new FileInputStream(src); 
			 XSSFWorkbook wb = new XSSFWorkbook(fis)) {
			XSSFSheet sheet = wb.getSheetAt(0);
			return sheet.getRow(1);
		}
	}
}
