package com.newtours.dataprovider;

public class FlightFinderPageData {

	private String type;
	private String passenger;
	private String departingFrom;
	private String date;
	private String month;
	private String arrivingIn;
	private String returnDate;
	private String returnMonth;
	private String serviceClass;
	private String airline;

	public String getType(){
		return type;
	}
	
	public String getPassenger(){
		return passenger;
	}
	
	public String getDepartingFrom(){
		return departingFrom;
	}
	
	public String getDate(){
		return date;
	}
	
	public String getMonth(){
		return month;
	}
	
	public String getArrivingIn(){
		return arrivingIn;
	}
	
	public String getReturnDate(){
		return returnDate;
	}
	
	public String getReturnMonth(){
		return returnMonth;
	}

	public String getServiceClass(){
		return serviceClass;
	}

	public String getAirline(){
		return airline;
	}
	
	public void setType(String type){
		this.type=type;
	}
	
	public void setPassenger(String passenger){
		this.passenger=passenger;
	}
	
	public void setDepartingFrom(String departingFrom){
		this.departingFrom=departingFrom;
	}
	
	public void setDate(String date){
		this.date=date;
	}
	
	public void setMonth(String month){
		this.month=month;
	}
	
	public void setArrivingIn(String arrivingIn){
		this.arrivingIn=arrivingIn;
	}
	
	public void setReturnDate(String returnDate){
		this.returnDate=returnDate;
	}
	
	public void setReturnMonth(String returnMonth){
		this.returnMonth=returnMonth;
	}

	public void setServiceClass(String serviceClass){
		this.serviceClass=serviceClass;
	}

	public void setAirline(String airline){
		this.airline=airline;
	}
	
}

